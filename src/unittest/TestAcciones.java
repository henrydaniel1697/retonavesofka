package unittest;

import junit.framework.TestCase;
import program.naves.NoTripuladas;
import program.naves.Tripuladas;
import program.naves.VehiculosLanzadera;


public class TestAcciones extends TestCase {

    private VehiculosLanzadera vehiculosLanzadera;
    private NoTripuladas noTripuladas;
    private Tripuladas tripuladas;

    public void test1(){
        vehiculosLanzadera = new VehiculosLanzadera("","", 0,0,0,"",0);
    }
    public void test2(){
        noTripuladas = new NoTripuladas("","", 0,0,"");
    }
    public void test3(){
        tripuladas = new Tripuladas("","",0, 0,0,"");
    }

    public void testNavesEspacialesTripuladas(){
        test1();
        int km = 100;
        assertEquals(vehiculosLanzadera.despegar(), "Acción 1: Ignición");
        assertEquals(vehiculosLanzadera.entrarEnOrbita(km),"Acción 2: " + "Interestelar a " + km + " kms");
    }

    public void testNavesNoTripuladas(){
        test2();
        int km = 100;
        assertEquals(noTripuladas.despegar(),"Acción 1: Accionar");
        assertEquals(noTripuladas.entrarEnOrbita(100),"Acción 2: Descanso a " + km + " kms");
    }

    public void testNavesVehiculosLanzadera(){
        test3();
        assertEquals(tripuladas.despegar(),"Acción 1: Despegue");
        assertEquals(tripuladas.aterrizar(),"Acción 2: Asegurado");
    }



}
