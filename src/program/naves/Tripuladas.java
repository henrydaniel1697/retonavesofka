package program.naves;

import javax.swing.*;

//Clase hija de Naves con una variable extra.
public class Tripuladas extends Naves{

    protected double potencia;
    public Tripuladas(String nombre, String tipoDeNave, double potencia, double altura, double peso, String pais){
        super(nombre, tipoDeNave, altura, peso, pais);
        this.potencia = potencia;
    }

    //Sobrecarga de metodo
    @Override
    public String despegar(){
        JOptionPane.showMessageDialog(null, "Acción 1: Despegue");
        return "Acción 1: Despegue";
    }

    //Sobreescritura de metodo
    @Override
    public String aterrizar(){
        JOptionPane.showMessageDialog(null, "Acción 2: Asegurado");
        return "Acción 2: Asegurado";
    }
    public void setPotencia(double potencia) {
        this.potencia = potencia;
    }
}
