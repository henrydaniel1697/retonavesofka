package program.naves;

import javax.swing.*;

//Clase hija de program.naves, con dos variales extra.
public class VehiculosLanzadera extends Naves{

    protected double capacidadDeCarga;
    protected double potencia;

    public VehiculosLanzadera(String nombre, String tipoDeNave, double potencia, double altura, double peso, String pais, double capacidadDeCarga){
        super(nombre, tipoDeNave, altura, peso, pais);
        this.capacidadDeCarga = capacidadDeCarga;
        this.potencia = potencia;
    }

    //Sobrecarga de metodo
    @Override
    public String despegar(){
        JOptionPane.showMessageDialog(null,"Acción 1: Ignición");
        return "Acción 1: Ignición";
    }

    //Sobreescritura de metodo
    public String entrarEnOrbita(int km){
        JOptionPane.showMessageDialog(null,"Acción 2: " + "Interestelar a " + km + " kms");
        return "Acción 2: " + "Interestelar a " + km + " kms";
    }

    public void setCapacidadDeCarga(double capacidadDeCarga) {
        this.capacidadDeCarga = capacidadDeCarga;
    }

    public void setPotencia(double potencia) {
        this.potencia = potencia;
    }
}
