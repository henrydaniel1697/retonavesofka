package program.naves;

import javax.swing.*;

//Clase hija de Naves
public class NoTripuladas extends Naves{

    public NoTripuladas(String nombre, String tipoDeNave, double altura, double peso, String pais){
        super(nombre, tipoDeNave, altura, peso, pais);
    }

    //Sobrecarga de metodo
    @Override
    public String despegar(){
        JOptionPane.showMessageDialog(null,"Acción 1: Accionar");
        return "Acción 1: Accionar";
    }

    //Sobreescritura de metodo
    public String entrarEnOrbita(int km){
        JOptionPane.showMessageDialog(null,"Acción 2: Descanso a " + km + " kms" );
        return "Acción 2: Descanso a " + km + " kms";
    }

}
