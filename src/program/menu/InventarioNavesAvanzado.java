package program.menu;

import program.naves.Naves;

import javax.swing.*;
import java.util.ArrayList;
//Clase para la busqueda avanzada de program.naves, se pueden buscar con diferentes filtros.
public class InventarioNavesAvanzado{

    public void ingresarMenu(ArrayList<Naves> listaNaves) {

        Naves resultado;

        int opcionInventario;
        String nombreDeNave;
        String nombreDePais;
        double altura;
        double peso;
        double mayor;
        double menor;

        opcionInventario = Integer.parseInt(JOptionPane.showInputDialog("""
                Selecciona el filtro que deseas:
                1.- Nombre.
                2.- Pais de origen.
                3.- Por altura (Metros aprox.)
                4.- Por peso (Toneladas aprox.)"""));

        switch (opcionInventario) {
            case 1 -> {
                nombreDeNave = JOptionPane.showInputDialog("Escribe el nombre de la nave");
                for (Naves naves : listaNaves) {

                    if (naves.getNombre().toLowerCase().contains(nombreDeNave.toLowerCase())) {

                        resultado = naves;
                        JOptionPane.showMessageDialog(null, "Nombre: " + resultado.getNombre() + "\nTipo de nave: " + resultado.getTipoDeNave() + "\nAltura: " + resultado.getAltura() + " m" + "\nPeso: " + resultado.getPeso() + " ton" + "\nPais: " + resultado.getPais());

                    }

                }
                JOptionPane.showMessageDialog(null, "La busqueda ha finalizado, vuelves al program.menu principal.");
            }
            case 2 -> {
                nombreDePais = JOptionPane.showInputDialog("Escribe nombre del pais de origen de la nave:");
                for (Naves naves : listaNaves) {

                    if (naves.getPais().toLowerCase().contains(nombreDePais.toLowerCase())) {

                        resultado = naves;
                        JOptionPane.showMessageDialog(null, "Nombre: " + resultado.getNombre() + "\nTipo de nave: " + resultado.getTipoDeNave() + "\nAltura: " + resultado.getAltura() + " m" + "\nPeso: " + resultado.getPeso() + " ton" + "\nPais: " + resultado.getPais());
                    }

                }
                JOptionPane.showMessageDialog(null, "La busqueda ha finalizado, vuelves al program.menu principal.");
            }
            case 3 -> {
                altura = Double.parseDouble(JOptionPane.showInputDialog("Escribe la altura (Metros):"));
                mayor = altura + 15;
                menor = altura - 15;
                for (Naves naves : listaNaves) {

                    if (naves.getAltura() <= mayor && naves.getAltura() >= menor) {
                        resultado = naves;
                        JOptionPane.showMessageDialog(null, "Nombre: " + resultado.getNombre() + "\nTipo de nave: " + resultado.getTipoDeNave() + "\nAltura: " + resultado.getAltura() + " m" + "\nPeso: " + resultado.getPeso() + " ton" + "\nPais: " + resultado.getPais());
                    }

                }
                JOptionPane.showMessageDialog(null, "La busqueda ha finalizado, vuelves al program.menu principal.");
            }
            case 4 -> {
                peso = Double.parseDouble(JOptionPane.showInputDialog("Escribe el peso de la nave (Toneladas):"));
                mayor = peso + 500;
                menor = peso - 500;
                for (Naves naves : listaNaves) {

                    if (naves.getPeso() <= mayor && naves.getPeso() >= menor) {
                        resultado = naves;
                        JOptionPane.showMessageDialog(null, "Nombre: " + resultado.getNombre() + "\nTipo de nave: " + resultado.getTipoDeNave() + "\nAltura: " + resultado.getAltura() + " m" + "\nPeso: " + resultado.getPeso() + " ton" + "\nPais: " + resultado.getPais());
                    }

                }
                JOptionPane.showMessageDialog(null, "La busqueda ha finalizado, vuelves al program.menu principal.");
            }
        }

        if (opcionInventario <= 0 || opcionInventario >= 5){
            JOptionPane.showMessageDialog(null,"Opción incorrecta, devuelta al program.menu principal");
        }
        }

}

