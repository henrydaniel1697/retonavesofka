package program.menu;

import program.naves.Naves;
import program.naves.NoTripuladas;
import program.naves.Tripuladas;
import program.naves.VehiculosLanzadera;

import javax.swing.*;
import java.util.ArrayList;

public class AgregarNave{

    int opcionInventario;
    String nombre;
    String tipoDeNave;
    double altura;
    double peso;
    String pais;
    double potencia;
    double capacidadDeCarga;

    public String crearNave(ArrayList<Naves> listaNaves) {

        opcionInventario = Integer.parseInt(JOptionPane.showInputDialog("""
                ¿Qué tipo de nave quieres agregar?
                1.- No tripulada
                2.- Tripulada
                3.- Vehiculo Lanzadera"""));

        switch (opcionInventario) {
            case 1 -> {
                do {
                    nombre = JOptionPane.showInputDialog("Ingresa el nombre de la nave");
                } while (nombre.equalsIgnoreCase(""));
                tipoDeNave = "Nave No Tripulada";
                do {
                    altura = Double.parseDouble(JOptionPane.showInputDialog("Ingresa la altura de la nave (Metros)"));
                } while (altura <= 0);
                do {
                    peso = Double.parseDouble(JOptionPane.showInputDialog("Ingresa el peso de la nave (Toneladas)"));
                } while (peso <= 0);
                do {
                    pais = JOptionPane.showInputDialog("Ingresa el pais de origen");
                } while (pais.equalsIgnoreCase(""));
                NoTripuladas nuevaNaveNT = new NoTripuladas(nombre, tipoDeNave, altura, peso, pais);
                nuevaNaveNT.setNombre(nombre);
                nuevaNaveNT.setTipoDeNave(tipoDeNave);
                nuevaNaveNT.setAltura(altura);
                nuevaNaveNT.setPeso(peso);
                nuevaNaveNT.setPais(pais);
                JOptionPane.showMessageDialog(null, "La información de la nave agregada es:" +
                        "\nNombre: " + nombre + "\nTipo de nave: " + tipoDeNave + "\nAltura: " + altura + "\nPeso: " + peso
                        + "\nPais: " + pais);
                listaNaves.add(new Naves(nuevaNaveNT.getNombre(),
                        nuevaNaveNT.getTipoDeNave(),
                        nuevaNaveNT.getAltura(),
                        nuevaNaveNT.getPeso(),
                        nuevaNaveNT.getPais()));
            }
            case 2 -> {
                do {
                    nombre = JOptionPane.showInputDialog("Ingresa el nombre de la nave");
                } while (nombre.equalsIgnoreCase(""));
                do {
                    potencia = Double.parseDouble(JOptionPane.showInputDialog("Ingresa la potencia de la nave"));
                } while (potencia <= 0);
                tipoDeNave = "Nave tripulada";
                do {
                    altura = Double.parseDouble(JOptionPane.showInputDialog("Ingresa la altura de la nave (Metros)"));
                } while (altura <= 0);
                do {
                    peso = Double.parseDouble(JOptionPane.showInputDialog("Ingresa el peso de la nave (Toneladas)"));
                } while (peso <= 0);
                do {
                    pais = JOptionPane.showInputDialog("Ingresa el pais de origen");
                } while (pais.equalsIgnoreCase(""));
                Tripuladas nuevaNaveT = new Tripuladas(nombre, tipoDeNave, potencia, altura, peso, pais);
                nuevaNaveT.setNombre(nombre);
                nuevaNaveT.setPotencia(potencia);
                nuevaNaveT.setTipoDeNave(tipoDeNave);
                nuevaNaveT.setAltura(altura);
                nuevaNaveT.setPeso(peso);
                nuevaNaveT.setPais(pais);
                JOptionPane.showMessageDialog(null, "La información de la nave agregada es:" + "\nPotencia: " + potencia +
                        "\nNombre: " + nombre + "\nTipo de nave: " + tipoDeNave + "\nAltura: " + altura + "\nPeso: " + peso
                        + "\nPais: " + pais);
                listaNaves.add(new Naves(nuevaNaveT.getNombre(),
                        nuevaNaveT.getTipoDeNave(),
                        nuevaNaveT.getAltura(),
                        nuevaNaveT.getPeso(),
                        nuevaNaveT.getPais()));
            }
            case 3 -> {
                do {
                    nombre = JOptionPane.showInputDialog("Ingresa el nombre de la nave");
                } while (nombre.equalsIgnoreCase(""));
                do {
                    potencia = Double.parseDouble(JOptionPane.showInputDialog("Ingresa la potencia de la nave"));
                } while (potencia <= 0);
                tipoDeNave = "Vehiculo Lanzadera";
                do {
                    altura = Double.parseDouble(JOptionPane.showInputDialog("Ingresa la altura de la nave (Metros)"));
                } while (altura <= 0);
                do {
                    peso = Double.parseDouble(JOptionPane.showInputDialog("Ingresa el peso de la nave (Toneladas)"));
                } while (peso <= 0);
                do {
                    pais = JOptionPane.showInputDialog("Ingresa el pais de origen");
                } while (pais.equalsIgnoreCase(""));
                do {
                    capacidadDeCarga = Double.parseDouble(JOptionPane.showInputDialog("Ingresa la capacidad de carga"));
                } while (capacidadDeCarga <= 0);
                VehiculosLanzadera nuevaNaveVL = new VehiculosLanzadera(nombre, tipoDeNave, potencia, altura, peso, pais, capacidadDeCarga);
                nuevaNaveVL.setNombre(nombre);
                nuevaNaveVL.setPotencia(potencia);
                nuevaNaveVL.setTipoDeNave(tipoDeNave);
                nuevaNaveVL.setAltura(altura);
                nuevaNaveVL.setPeso(peso);
                nuevaNaveVL.setPais(pais);
                nuevaNaveVL.setCapacidadDeCarga(capacidadDeCarga);
                JOptionPane.showMessageDialog(null, "La información de la nave agregada es:" + "\nPotencia: " + potencia +
                        "\nNombre: " + nombre + "\nTipo de nave: " + tipoDeNave + "\nAltura: " + altura + "\nPeso: " + peso
                        + "\nPais: " + pais + "\nCapacidad de carga: " + capacidadDeCarga);
                listaNaves.add(new Naves(nuevaNaveVL.getNombre(),
                        nuevaNaveVL.getTipoDeNave(),
                        nuevaNaveVL.getAltura(),
                        nuevaNaveVL.getPeso(),
                        nuevaNaveVL.getPais()));
            }
        }

        if (opcionInventario <= 0 || opcionInventario >= 4){
            JOptionPane.showMessageDialog(null,"Opción incorrecta, devuelta al program.menu principal");
        }
        return nombre;
    }

}
